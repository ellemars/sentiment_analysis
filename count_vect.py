#!/usr/bin/python
# -*- coding: utf-8 -*-

"""CountVectorizer module"""

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression



class GetCountVect():
    countv = CountVectorizer(ngram_range=(1,3))
    lr = LogisticRegression()

    def __init__(self, positive_file, negative_file):
        pos_tw = open(positive_file, 'r', encoding='utf8')
        neg_tw = open(negative_file, 'r', encoding='utf8')
        #makes dicts of tweets' texts and labels
        tweets = []
        labels = []
        self._append_tweets_and_labels(tweets,labels,pos_tw)
        self._append_tweets_and_labels(tweets,labels,neg_tw)

        #trains countvectorizer on tweets' features
        count_features = self.countv.fit_transform(tweets)
        self.lr.fit(count_features, labels)

    def _append_tweets_and_labels(self, twts, lbls, file):
        """Parses tweet to extract text and its label"""
        for line in file:
            t = line.strip().split(',')
            label = int(t[-1])
            tweet = ','.join(t[:-1])
            lbls.append(label)
            twts.append(tweet)
        
    def count_calc(self, text):
        """Main function of the module
        It predicts the sentiment assessment of the text 
        using the countvectorizer model trained on the corpus of pos/neg tweets
            Args:
                text
            Returns:
                sentiment assessment of the text
        """
        prep_text = [text]
        text_countv_features = self.countv.transform(prep_text)
        pred = self.lr.predict(text_countv_features)
        result = pred.tolist()
        
        return str(result).strip('[]')
