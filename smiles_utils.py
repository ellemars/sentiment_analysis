#!/usr/bin/python
# -*- coding: utf-8 -*-

_smiles_pos = [')', ':)', ':D', ':d', ';)', ';D', ';d', '^^', '^_^', '^-^', '(:', '=)', '=D', '(=', ':P', ':p','=P','=p',';P',';p', ';-)',':-)','=-)']
_smiles_neg = ['(', ':(', 'D:', 'D=', ';(', '><', ':/', '=(', ':-(', '=-(', ';-(']

def getPositiveSmilesList():
    return _smiles_pos

def getNegativeSmilesList():
    return _smiles_neg

def removeSmile(one_list):
    """Removes smiles from list of words"""
    for element in one_list:
        if element in _smiles_pos + _smiles_neg:
            one_list.remove(element)
    return one_list


