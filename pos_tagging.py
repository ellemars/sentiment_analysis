#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Part-of-speech tagging"""

import pymorphy2

def getPosTags(word):
    p = pymorphy2.MorphAnalyzer().parse(str(word))[0]
    return p.tag.POS