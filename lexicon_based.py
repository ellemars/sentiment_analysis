#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Lexicon based method"""


import potts_tokenizer
import normalize_word
import pos_tagging
import smiles_utils


class GetLexiconBasedAssessment():
    def __init__(self, positive_words, negative_words):
        pos_words = open(positive_words, 'r')
        neg_words = open(negative_words, 'r')

        self.sentiment_words_dict = {}

        for word in pos_words:
            strip_word = word.strip('\n')
            self.sentiment_words_dict[strip_word] = { 'sentiment' : '1' , 'word' : strip_word }

        for word in neg_words:
            strip_word = word.strip('\n')
            self.sentiment_words_dict[strip_word] = { 'sentiment' : '-1' , 'word' : strip_word }

        for smile in smiles_utils.getPositiveSmilesList():
            self.sentiment_words_dict[smile] = { 'sentiment' : '1' , 'word' : smile }

        for smile in smiles_utils.getNegativeSmilesList():
            self.sentiment_words_dict[smile] = { 'sentiment' : '-1' , 'word' : smile }


    def _splitTextToSentences(self, text):
        """Splits text to sentences
        Replace punctuational signs for further splitting by comma.
                Args:
                    text as a string
                Returns:
                    sentences of text as strings
        """
        prep_text = text.replace('!', '.').replace('?','.').replace(';','.')
        split_text = prep_text.split('.')
        splitted_text = [sentence.strip() for sentence in split_text if sentence.strip()]
        return splitted_text



    def _tokenizeSentencesToWords(self,text):
        """Splits text(one sentence) to words (tokenization)
        Potts_tokenizer is used, because it extracts words and smiles as well. 
            Args:
                text: text as one string
            Returns:
                tokenized_text: list of lists of all words and smiles of text (one list = one sentence)
        """
        splitted_text = self._splitTextToSentences(text)
        tokenized_text = []
        for sentence in splitted_text:
            tokenized_text.append(potts_tokenizer.getWords(str(sentence)))
        return tokenized_text



    def _getLemmasOfSentence(self,tokenized_sentence):
        """Gets lemmas of words in one sentence
        normalize_word module is used. It deletes pinctuation, integers, stop words; 
        uses Yandex.Speller to correct word spelling.
        As a result, it returns lemmas of sentence. 
        It also checks 'не' in sentence and returns it in case of following by verb, infinitive or adjective.
                Args:
                    tokenized_sentence: list of words
                Returns:
                    list_of_lemmas: list of lemmas
        """
        list_of_lemmas = []
        i = 0
        for word in tokenized_sentence:
            if word == 'не' and any([pos_tagging.getPosTags(tokenized_sentence[i+1]) == 'VERB', 
                                    pos_tagging.getPosTags(tokenized_sentence[i+1]) == 'INFN',
                                    pos_tagging.getPosTags(tokenized_sentence[i+1]) == 'ADJF']):
                list_of_lemmas.append('не')
            elif word != 'не' and normalize_word.getNormalizedWord(word) != None:
                list_of_lemmas.append(normalize_word.getNormalizedWord(word))
            i+=1
        return list_of_lemmas




    def _getLemmasOfText(self, tokenized_sentences):
        """Gathers all lemmas' lists in one list
                Args:
                    tokenized_sentence: list of lemmas of one sentence
                Returns:
                    lemmas: list of all lists of lemmas
        """
        lemmas_of_text = []
        for sentence in tokenized_sentences:
            lemmas_of_text.append(self._getLemmasOfSentence(sentence))
        return lemmas_of_text



    def _getSentimentOfSentence(self, lemmas_of_one_sentence):
        """Adds a sentiment tag to lemmas
        It checks the existence of word or smile in dictionary
            Args:
                tokenized_sentence: list of lemmas of one sentence
            Returns:
                lemmas: dicts of lemmas with sentiment tags
        """
        sentence_sentiment = []
        i = 0
        for lemma in lemmas_of_one_sentence:
            if lemma in self.sentiment_words_dict:
                sentence_sentiment.append({'id': i,
                                            'sentiment' : self.sentiment_words_dict[lemma]['sentiment'], 
                                            'word' : lemma})
                i += 1
        if len(sentence_sentiment) != 0:
            return sorted(sentence_sentiment, key=lambda k: k['id'])


    def _getAllSentiment(self, lemmas_of_text):
        """Gathers all dicts of words of all sentences in one list
                Args:
                    list of lists (lemmas of sentence)
                Returns:
                    list of lemmas' dicts with sentiment tags
        """
        sentences_sentiment = []
        for sentence in lemmas_of_text:
            sentFromSentence = self._getSentimentOfSentence(sentence)
            if sentFromSentence != None:
                sentences_sentiment.append(sentFromSentence)
        return sentences_sentiment




    def _checkNegation(self, sentence_dict):
        """Changes the sentiment assessment of verb (if there is 'не' before it)
                Args:
                    sent: dict of words of one sentence (with sentiment)
                Returns:
                    p: dict of words with changed sentiment
        """
        if len(sentence_dict) > 1:
            sorted_list = sorted(sentence_dict, key=lambda k: k['id'])
            for word in sorted_list:
                if word['word'] == 'не':
                    word['sentiment'] = '0'
                    i = 0
                    sorted_list[i+1]['sentiment'] = str(int(sorted_list[i+1]['sentiment'])*(-1))
        else:
            sorted_list = sentence_dict
        return sorted_list

    def _countSentenceSentiment(self, words_of_sentence_with_sentiment):
        """Calculates the sentiment assessment of one sentence
                Args: 
                    words_of_sentence_with_sentiment: dict of words with sentiment assessment of one sentence
                Returns:
                    sentence_sentiment: integer: sentiment assessment of one sentence 
        """
        sentiment_dict_of_words = self._checkNegation(words_of_sentence_with_sentiment)
        sentence_sentiment = 0
        if len(sentiment_dict_of_words) > 0:
            for element in sentiment_dict_of_words:
                sentence_sentiment += int(element['sentiment'])
        return sentence_sentiment


    def countTextSentiment(self, text):
        """Calculates the sentiment assessment of the text
        The main function of the module to get sentiment assessment of the whole text.
        It gathers sentiment assessments of all sentences and calculates the sentiment assessment of the whole text by summing its sentence sentiment.
                Args: 
                    text: text
                Returns:
                    sentence_sentiment: 1(positive), -1(negative), 0(neutral)
        """
        sentiment_of_sentences = self._getAllSentiment(self._getLemmasOfText(self._tokenizeSentencesToWords(text)))
        sentiment_assessment = 0
        for sentence in sentiment_of_sentences:
            sentiment_assessment += self._countSentenceSentiment(sentence)
        if sentiment_assessment >= 1:
            return ('1')
        elif sentiment_assessment <= -1:
            return ('-1')
        elif sentiment_assessment == 0:
            return ('0')


