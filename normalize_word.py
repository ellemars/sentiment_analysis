#!/usr/bin/python
# -*- coding: utf-8 -*-

import nltk
import pymorphy2
import json
import requests
from nltk.corpus import stopwords
from alphabet_detector import AlphabetDetector


def _isPunct(word):
    """Checks if the word is a punctuation mark"""
    puncList = ['.', ':', ',', ';', '-', '!', '?', "'", '"']
    if word in puncList:
        return True
    else:
        return False
                        

def _isInteger(word):
    """Checks if the word is an integer"""
    for letter in word:
        if letter.isdigit():
            return True
        else:
            return False
                    

def _isStopWord(word):
    """Checks if the word is a stopword, exceptions: не, хорошо, лучше"""
    if word in stopwords.words('russian') and any([word == 'не', word == 'хорошо', word == 'лучше']):
        return False
    elif word in stopwords.words('russian'):
        return True
    else:
        return False


def _correctWordSpelling(word):
    link = 'http://speller.yandex.net/services/spellservice.json/checkText?text='+ word
    """Corrects a misspelled word using Yandex.Speller"""
    headers = {'content-type': 'application/json; charset=utf-8'}
    r = requests.get(link, headers=headers)  #send request to Yandex.Speller
    s = str(r.json())
    if s == '[]':
        return word #Word is correct, not necessary to modify
    elif "," in s[s.find("['")+2:s.find("']")]: 
        return None
    else:
        m_word = s[s.find("['")+2:s.find("']")] #Find text(modified word) in brackets
        return m_word



def _getLemma(word): 
    """Gets a normalized form - lemma of a word"""
    ad = AlphabetDetector()
    morph = pymorphy2.MorphAnalyzer()
    if ad.only_alphabet_chars(str(word), 'CYRILLIC'): #Only cyrillic words are needed
        mrph = morph.parse(str(word))[0].normal_form
    else:
        mrph = str(word)
    return mrph


def getNormalizedWord(word):
    """Filter - returns not None results"""
    if word == "лол":
        return word
    elif (_isPunct(word) == False and _isInteger(word) == False and _isStopWord(word) == False and _correctWordSpelling(word) != None):
        return _getLemma(_correctWordSpelling(word))
    elif (_isPunct(word) == False and _isInteger(word) == False and _isStopWord(word) == False and _correctWordSpelling(word) == None):
        return word
    else:
        return None
