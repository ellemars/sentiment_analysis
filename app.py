#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import count_vect
import lexicon_based
import smiles_utils
import normalize_word
import potts_tokenizer

from flask import Flask, jsonify
from flask import request

app = Flask(__name__)


@app.route('/', methods = ['POST'])
def get_sent():
    """It takes text and returns the sentiment assessment of it
    calculated by lexicon_based method or countvectorizer method.
    It checks text length and chooses one of method: 
    if number of words after removing smiles, stopwords, punctuation marks <= 1 or == 2 words and one of words is 'не', 
    it conducts lexicon_based method, else: countvectorizer method
    """
    text = request.json['text']
    tokens = potts_tokenizer.getWords(text)
    norm_word = [normalize_word.getNormalizedWord(word) for word in tokens]
    words = []
    for word in norm_word:
        if word != None:
            words.append(word)
    smiles_utils.removeSmile(words)
    if len(words) <= 1:
        # loading dictionaries of positive and negative words
        lex_based = lexicon_based.GetLexiconBasedAssessment('input/dict_words/poz_normalized_dict_ru.csv', 'input/dict_words/neg_normalized_dict_ru.csv')
        sentiment_assessment = lex_based.countTextSentiment(text)
    elif len(words) == 2 and 'не' in words:
        # loading dictionaries of positive and negative words
        lex_based = lexicon_based.GetLexiconBasedAssessment('input/dict_words/poz_normalized_dict_ru.csv', 'input/dict_words/neg_normalized_dict_ru.csv')
        sentiment_assessment = lex_based.countTextSentiment(text)
    else:
        # loading dictionaries of positive and negative tweets
        get_countv = count_vect.GetCountVect('input/dict_of_tweets/positive.csv', 'input/dict_of_tweets/negative.csv')
        sentiment_assessment = get_countv.count_calc(text)
    return  jsonify({'text': text, 'sentiment': str(sentiment_assessment)})



if __name__ == '__main__':
    app.run()