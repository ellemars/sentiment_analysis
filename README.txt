Russian Text Sentiment analysis API

==================================================

LexiconBased and CountVectorizer methods are used depending on the length and content of input text.


Yulia Rubtcova's corpus of tweets (http://study.mokoron.com/) is used in CountVectorizer method.

Translated and extended sentiment lexicon of Minqing Hu and Bing Liu (http://www.cs.uic.edu/~liub/FBS/opinion-lexicon-English.rar) is used in FindInVocabulary method.

---------------------------------------------------
You need to have pre-installed:
- nltk
- pymorphy2
- alphabet_detector
- scikit-learn
- flask
--------------------------------------------------

Use python app.py command to run the program.
Use any HTTP request tool to send POST request like {"text": "print your text here"} to get the sentiment assessment of your text.
