#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import pos_tagging



class testPosTagging(unittest.TestCase):
    # checks the work of pos-tagging
    def test_getPosTags(self):
        self.assertEqual(pos_tagging.getPosTags('красивый'), 'ADJF')
        self.assertIsNone(pos_tagging.getPosTags('good'))


m = testPosTagging()
suite = unittest.TestLoader().loadTestsFromModule(m)
unittest.TextTestRunner().run(suite)



