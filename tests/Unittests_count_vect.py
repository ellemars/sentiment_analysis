#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import count_vect

get_countv = count_vect.GetCountVect('input/dict_of_tweets/positive.csv', 'input/dict_of_tweets/negative.csv')


#tests work of countvectorizer module, returns sentiment assessment of text
class testCountVectorizerModule(unittest.TestCase):
    def test_count_calc(self):
        self.assertEqual(get_countv.count_calc('Я не люблю яблоки'),'-1')



m = testCountVectorizerModule()
suite = unittest.TestLoader().loadTestsFromModule(m)
unittest.TextTestRunner().run(suite)