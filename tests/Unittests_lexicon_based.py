#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import lexicon_based

lex_based = lexicon_based.GetBagOfWordsResult('input/dict_words/poz_normalized_dict_ru.csv', 'input/dict_words/neg_normalized_dict_ru.csv')



class testLexiconBasedModule(unittest.TestCase):
    # tests text to sentences splitting
    def test_splitTextToSentences(self):
        self.assertEqual(lex_based._splitTextToSentences('Я не люблю яблоки. Я люблю груши =)'), ['Я не люблю яблоки', 'Я люблю груши =)'])
        self.assertEqual(lex_based._splitTextToSentences('Супер! Я тоже хочу съездить в Венецию!'), ['Супер', 'Я тоже хочу съездить в Венецию'])
        
    # tests sentence to words splitting (tokenization)
    def test_tokenizeSentencesToWords(self):
        self.assertEqual(lex_based._tokenizeSentencesToWords('Я не люблю яблоки. Я люблю груши =)'), 
                        [['я', 'не', 'люблю', 'яблоки'], ['я', 'люблю', 'груши', '=)']])
    
    # tests lemmas of sentence getting
    def test_getLemmasOfSentence(self):
        self.assertEqual(lex_based._getLemmasOfSentence(['я', 'не', 'люблю', 'яблоки']), ['не', 'любить', 'яблоко'])
    
    
    # tests adding of sentiment tags to words (checks whether the word in dictionary or not, returns only those words )
    def test_getSentimentOfSentence(self):
        self.assertEqual(lex_based._getSentimentOfSentence(['не', 'любить', 'яблоко' , '=)']),
                                    [{'id': 0, 'word': 'не', 'sentiment': '-1'}, {'id': 1, 'word': 'любить', 'sentiment': '1'}, {'id': 2, 'word': '=)', 'sentiment': '1'}])
    
    # tests negation checking
    def test_checkNegation(self):
        self.assertEqual(lex_based._checkNegation([{'id': 1,'sentiment': '1', 'word': 'любить'}, {'id': 0,'sentiment': '-1', 'word': 'не'}]), 
                                    [{'id': 0, 'sentiment': '0', 'word': 'не'}, {'id': 1, 'sentiment': '-1', 'word': 'любить'}])
    
    # tests sentence sentiment counting
    def test_countSentenceSentiment(self):
        self.assertEqual(lex_based._countSentenceSentiment([{'id': 1, 'sentiment': '1', 'word': 'любить'}, {'id': 0, 'sentiment': '-1', 'word': 'не'}]), -1)
        self.assertEqual(lex_based._countSentenceSentiment([{'id': 0,'sentiment': '1', 'word': 'любить'}, {'id': 1,'sentiment': '1', 'word': '=)'}]), 2)
    
    # tests text sentiment counting
    def test_countTextSentiment(self):
        self.assertEqual(lex_based.countTextSentiment('Я не люблю яблоки. Мне нравятся груши.'), '0')



m = testLexiconBasedModule()
suite = unittest.TestLoader().loadTestsFromModule(m)
unittest.TextTestRunner().run(suite)