#!/usr/bin/env python
# -*- coding: utf-8 -*-


import unittest
import potts_tokenizer



class testPottsTokenizer(unittest.TestCase):
    # checks the work of potts tokenizer
    def test_getWords(self):
        self.assertEqual(potts_tokenizer.getWords('Я бы хотел научиться играть на гитаре =)'), ['я', 'бы', 'хотел', 'научиться', 'играть', 'на', 'гитаре', '=)'])



m = testPottsTokenizer()
suite = unittest.TestLoader().loadTestsFromModule(m)
unittest.TextTestRunner().run(suite)