#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import normalize_word


class testNormalizeWordModule(unittest.TestCase):
    def test_isPunct(self):
        self.assertEqual(normalize_word._isPunct('красивый'), False)
        self.assertEqual(normalize_word._isPunct('!'), True)
        
    def test_isInteger(self):
        self.assertEqual(normalize_word._isInteger('супер'), False)
        self.assertEqual(normalize_word._isInteger('5'), True)
    
    def test_isStopWord(self):
        self.assertEqual(normalize_word._isStopWord('не'), False)
        self.assertEqual(normalize_word._isStopWord('плохой'), False)
        self.assertEqual(normalize_word._isStopWord('я'), True)

        
    def test_correctWordSpelling(self):
        self.assertEqual(normalize_word._correctWordSpelling('клссный'), 'классный')
        self.assertEqual(normalize_word._correctWordSpelling('классный'), 'классный')
        self.assertIsNone(normalize_word._correctWordSpelling('лжный')) #unclear 'ложный' or 'лыжный'
    
    def test_getLemma(self):
        self.assertEqual(normalize_word._getLemma('сложное'), 'сложный')
        self.assertEqual(normalize_word._getLemma('lol'), 'lol')
        
    def test_getNormalizedWord(self):
        self.assertIsNotNone(normalize_word.getNormalizedWord('классный'))
        self.assertIsNotNone(normalize_word.getNormalizedWord('клссный'))
        self.assertIsNone(normalize_word.getNormalizedWord('я'))
        

m = testNormalizeWordModule()
suite = unittest.TestLoader().loadTestsFromModule(m)
unittest.TextTestRunner().run(suite)




